import 'package:flim_app/extension/color_hex.dart';
import 'package:flim_app/utils/picture_util.dart';
import 'package:flutter/material.dart';

class MainHomePage extends StatefulWidget {
  @override
  _MainHomePageState createState() => _MainHomePageState();
}

class _MainHomePageState extends State<MainHomePage> {
  bool fetchingData = false;
  int _currentIndex = 0;
  Color bottomNavbarColor = Colors.white;
  Color active = Colors.black;
  Color inActive = Colors.grey;
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  Color activeColor = HexColor.fromHex('#3FC820');

  @override
  void initState() {
    super.initState();
    setState(() {
      _currentIndex = 0;
    });
  }

  Widget myTitle() {
   return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        LocalSvgImage(url: 'assets/images/svg/flim-white.svg', size: 40.0, color: Colors.white),
        Container(
            padding: const EdgeInsets.all(8.0), child: Text(''))
      ],
    );
  }

  Widget searchBox() {
    return Card(child: Container(
        padding: EdgeInsets.fromLTRB(15, 15, 15, 10),
        child: Column(children: <Widget>[
          TextField(decoration: InputDecoration(
            border: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey, width: 2.0)),
            hintText: 'Enter recipe',
            suffixIcon: IconButton(
              icon: Icon(
                Icons.search,
                color: Theme.of(context).primaryColorDark,
              ), onPressed: () {  },
            ),
          )),
          new Container(
            padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
            child: Align(
              alignment: Alignment.bottomRight,
              child: new GestureDetector(
                  child:  new Text("Advanced Search",
                    style: new TextStyle(color: Colors.blueAccent, fontSize: 12),
                    textAlign: TextAlign.end,),
                  onTap: () {
                    Navigator.pushNamed(context, '/activate-account-with-sms');
                  }),
            ),)
        ])
    ));
  }

  Widget widgetAddPadding(Widget input) {
    return new Padding(padding: EdgeInsets.all(5), child: input);
  }

  TableRow tableRow(String leftColumn, String rightColumn) {
      return TableRow( children: [
        Column(children:[
          widgetAddPadding(Align(alignment: Alignment.centerLeft, child: new Text(leftColumn)))
        ]),
        Column(children:[
          widgetAddPadding(Align(alignment: Alignment.centerLeft, child: new Text(rightColumn)))
        ])
      ]);
  }

  Widget calories() {
    return Card(child: Container(
        padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
        child: Column(children: <Widget>[
          Align(alignment: Alignment.topLeft,
              child: new Text("Calories", style: TextStyle(fontSize: 18),textAlign: TextAlign.start)),
          new Divider(),

          Table(
              border: TableBorder.all(),
              columnWidths: {0: FractionColumnWidth(.3), 1: FractionColumnWidth(.7)},
              children: [
                tableRow("Today", "2000 calories"),
                tableRow("Week", "4000 calories"),
                tableRow("Month", "5000 calories"),
                tableRow("Quarterly", "5000 calories"),
                tableRow("Yearly", "5000 calories")
              ]
          ),

        ])));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          backgroundColor: activeColor,
          elevation: 4,
          title: myTitle(),
        ),
        body: SingleChildScrollView(
            child: Stack(children: [
                new Container(
                    color: Theme.of(context).backgroundColor,
                    child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: new Stack(children: <Widget>[
                          new Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                searchBox(),
                                calories()

                        ])
                  ])))
        ])
        )
    );
  }
}
