import 'dart:convert';

import 'package:flim_app/extension/color_hex.dart';
import 'package:flim_app/model/recipe_model.dart';
import 'package:flim_app/utils/picture_util.dart';
import 'package:flim_app/utils/utils.dart';
import 'package:flim_app/widget/stateful/recipe/recipe_view.dart';
import 'package:flutter/material.dart';

class RecipePage extends StatefulWidget {
  @override
  _RecipePageState createState() => _RecipePageState();
}

class _RecipePageState extends State<RecipePage> {
  bool fetchingData = true;
  int _currentIndex = 0;
  Color bottomNavbarColor = Colors.white;
  Color active = Colors.black;
  Color inActive = Colors.grey;
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  Color activeColor = HexColor.fromHex('#3FC820');
  List<RecipeModel> listOfRecipes = [];
  List<RecipeModel> items = [];

  @override
  void initState() {
    super.initState();
    setState(() {
      _currentIndex = 0;
    });
    this.loadRecipeData().then((onValue) {
      print(onValue);
      if(onValue != null) {
        setState(() {
          listOfRecipes = parseJson(onValue);
          fetchingData = false;
          this.items.addAll(this.listOfRecipes);
        });
      } else {
        setState(() {
          fetchingData = false;
        });
      }
    });
  }

  Future<String> loadRecipeData() async {
    try {
      return await DefaultAssetBundle.of(context).loadString('assets/json/recipes.json');
    } catch (e) {
      return null;
    }
  }

  List<RecipeModel> parseJson(String response) {
    if (response == null) {
      return [];
    }

    final parsed = json.decode(response).cast<Map<String, dynamic>>();

    return parsed.map<RecipeModel>((json) => RecipeModel.fromJson(json)).toList();
  }

  Widget myTitle() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
            padding: const EdgeInsets.all(8.0), child: Text('Recipe', style: TextStyle(color: Colors.white),))
      ],
    );
  }

  Widget createCard(RecipeModel recipeModel){
    return Card(child: GestureDetector(child: Container(padding: EdgeInsets.all(10),
        child: Column(children: <Widget>[
          LocalPngORJPGImage(url: recipeModel.imageUrl, width: 150.0, height: 150.0),
          SizedBox(height: 10,),
          Text(recipeModel.name, style: TextStyle(fontSize: 20),),
        ],)
    ), onTap: () {
      //Navigator.of(context).pushNamed('/view_report', arguments: recipeModel);

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => RecipeViewPage(arguments: recipeModel),
        ),
      );
    },),);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          backgroundColor: activeColor,
          elevation: 4,
          title: myTitle(),
        ),
        body: new Container(
            padding: const EdgeInsets.all(10.0),
            color: Theme.of(context).backgroundColor,
            child: Stack(children: <Widget>[
              fetchingData ? new Center(child: Utils.spinner()):
                new ListView.builder(
                      itemCount: items.length,
                      itemBuilder: (BuildContext ctxt, int index) {
                        return createCard(items[index]);
                      }
                  )
            ])
         ));
  }


}