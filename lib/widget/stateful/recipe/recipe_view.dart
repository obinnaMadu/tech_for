import 'package:flim_app/extension/color_hex.dart';
import 'package:flim_app/model/recipe_model.dart';
import 'package:flim_app/utils/picture_util.dart';
import 'package:flim_app/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

class RecipeViewPage extends StatefulWidget {

  final RecipeModel arguments;

  RecipeViewPage({this.arguments});

  @override
  _RecipeViewPageState createState() => _RecipeViewPageState(arguments: arguments);
}

class _RecipeViewPageState extends State<RecipeViewPage> {
  bool fetchingData = true;
  //RecipeModel recipeModel;
  Color activeColor = HexColor.fromHex('#3FC820');
  final RecipeModel arguments;

  _RecipeViewPageState({this.arguments});

  @override
  void initState() {
    super.initState();
    setState(() {
      fetchingData = false;
    });
  }

  Widget myTitle() {
    return
        Container(
            padding: const EdgeInsets.all(8.0), child: Text('Recipe', style: TextStyle(color: Colors.white),));
  }


  Widget createCard(RecipeModel recipeModel){
    return Card(child: Container(padding: EdgeInsets.all(10),
        child: Column(children: <Widget>[
          LocalPngORJPGImage(url: recipeModel.imageUrl, width: 150.0, height: 150.0),
        ])
    )
    );
  }

  Widget myText(String title){
    return Container(padding: EdgeInsets.fromLTRB(0, 10, 0, 10),child: Text(title));
  }

  @override
    Widget build(BuildContext context) {

      return Scaffold(
          appBar: AppBar(
            backgroundColor: activeColor,
            elevation: 4,
            title: myTitle(),
          ),
          body: new Container(
              padding: const EdgeInsets.all(10.0),
              color: Theme.of(context).backgroundColor,
              child: SingleChildScrollView(
                  child: Stack(children: <Widget>[
                fetchingData ? new Center(child: Utils.spinner()):
                new Column(children: <Widget>[
                  createCard(arguments),
                  myText("Ingedients"),
                  Html(data: arguments.ingredients,),
                  myText("Methods"),
                  Html(data: arguments.method,)
                ],)
              ]))
        )
      );
  }
}