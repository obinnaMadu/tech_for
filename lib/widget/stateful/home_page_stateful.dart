
import 'package:flim_app/extension/color_hex.dart';
import 'package:flim_app/fonts/my_flutter_app_icons.dart';
import 'package:flim_app/my_enum/navigation_enum.dart';
import 'package:flim_app/widget/stateful/main_home_page.dart';
import 'package:flim_app/widget/stateful/recipe/recipe.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart' as foundation;
import 'package:flutter/material.dart';

bool get isIos => foundation.defaultTargetPlatform == foundation.TargetPlatform.iOS;

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool fetchingData = false;
  int _currentIndex = 0;
  Color bottomNavbarColor = Colors.white;
  Color activeColor = HexColor.fromHex('#3FC820');

  //const Color(3FC820);
  Color inActiveColor = Colors.grey;

  @override
  void initState() {
    super.initState();
    setState(() {
      _currentIndex = 0;
    });
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  final List<Widget> _children = [
    MainHomePage(),
    RecipePage(),
    MainHomePage(),
    MainHomePage()
  ];


  Widget myText({String name, int index}) {
    bool active = checkButtonActive(index);
    return new Text(name, style: TextStyle(color: active ? activeColor : inActiveColor, fontSize: active ? 13 : 12));
  }

  Widget myIcon(MyNavigationEnum name) {

    IconData icon;
    bool active;

    if(name == MyNavigationEnum.home) {
      icon = checkButtonActive(0) ? MyFlutterApp.home_active : MyFlutterApp.home;
      active = checkButtonActive(0);
    } else if(name == MyNavigationEnum.recipe) {
      icon = checkButtonActive(1) ? MyFlutterApp.recipe_active:  MyFlutterApp.recipe;
      active = checkButtonActive(1);
    } else if(name == MyNavigationEnum.shop) {
      icon = checkButtonActive(2) ? MyFlutterApp.shop_active:  MyFlutterApp.shop;
      active = checkButtonActive(2);
    } else if(name == MyNavigationEnum.profile) {
      icon = checkButtonActive(3) ? MyFlutterApp.user_active:  MyFlutterApp.user;
      active = checkButtonActive(3);
    }
    return new Icon(icon, color: active ? activeColor : inActiveColor);
  }

  @override
  Widget build(BuildContext context) {
      return Scaffold(
        body: _children[_currentIndex],
        bottomNavigationBar: BottomNavigationBar(
          elevation: 0,
          backgroundColor: bottomNavbarColor,
          onTap: onTabTapped,
          currentIndex: _currentIndex,
          items: [
            BottomNavigationBarItem(
              icon: myIcon(MyNavigationEnum.home),
              title: myText(name: 'Home', index: 0),
            ),
            BottomNavigationBarItem(
              icon: myIcon(MyNavigationEnum.recipe),
              title: myText(name: 'Recipe', index: 1),
            ),
            BottomNavigationBarItem(
                icon: myIcon(MyNavigationEnum.shop),
                title: myText(name: 'Shop', index: 2),
            ),
            BottomNavigationBarItem(
                icon: myIcon(MyNavigationEnum.profile),
                title: myText(name: 'Profile', index: 3))
          ],
        ),
      );
    }

  checkAndReturnColor(int index) {
    return _currentIndex == index ? activeColor: inActiveColor;
  }


  checkButtonActive(int index) {
    return _currentIndex == index ? true: false;
  }
}