class RecipeModel {
  final int id;
  final String name;
  final String ingredients;
  final String imageUrl;
  final String method;
  final String duration;
  final String calories;

  RecipeModel({this.id, this.name, this.ingredients, this.imageUrl, this.method, this.duration, this.calories});

  factory RecipeModel.fromJson(Map<String, dynamic> json) {
    return new RecipeModel(
      id: json['id'] as int,
      name: json['name'] as String,
      ingredients: json['ingredients'] as String,
      imageUrl: json['imageUrl'] as String,
      method: json['method'] as String,
      duration: json['duration'] as String,
      calories: json['calories'] as String,
    );
  }
}