import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MyPictureUtil {
  static svg({src, size, color}) {
    return new SvgPicture.asset(
      src,
      semanticsLabel: 'test',
      width: size,
      height: size,
      color: color != null ? color : null,
    );
  }
}

class LocalSvgImage extends StatelessWidget {

  String url;
  double size;
  Color color;
  LocalSvgImage({ @required this.url, @required this.size, this.color});

  @override
  Widget build(BuildContext context) {
    String logoAssetName = url;
    return new SvgPicture.asset(
      logoAssetName,
      semanticsLabel: 'Tradecraft security Logo',
      width: this.size,
      height: this.size,
      color: this.color != null ? this.color : null,
    );
  }
}


class LocalPngORJPGImage extends StatelessWidget {

  String url;
  double width;
  double height;
  LocalPngORJPGImage({ @required this.url, @required this.width, this.height});

  @override
  Widget build(BuildContext context) {
    return new Image(image: AssetImage(url), width: width, height: height);
  }
}