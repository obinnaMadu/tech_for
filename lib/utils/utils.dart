import 'package:flim_app/extension/color_hex.dart';
import 'package:flutter/material.dart';

class Utils {
  static Widget spinner() {
    Color activeColor = HexColor.fromHex('#3FC820');
    return new CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(activeColor));
  }
}